package main

import (
	"log"
	"net/http"
	"github.com/gorilla/websocket"
	"fmt"
	"os"
	"strconv"
)

// Define our message object
type Message struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Message  string `json:"message"`
}

var Clients = make(map[*websocket.Conn]bool) // connected clients
var Broadcast = make(chan Message)           // broadcast channel
var Upgrade = websocket.Upgrader{}           // Configure the upgrader

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := Upgrade.Upgrade(w, r, nil)
	HandleErrors(err, "Can't upgrade GET request to a websocket")

	// Make sure we close the connection when the function returns
	defer ws.Close()

	// Register our new client
	Clients[ws] = true

	for {
		var msg Message

		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("error: %v", err)
			delete(Clients, ws)
			break
		}
		// Send the newly received message to the broadcast channel
		Broadcast <- msg
	}
}

func handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-Broadcast

		// Send it out to every client that is currently connected
		for client := range Clients {
			go func(client *websocket.Conn) {
				err := client.WriteJSON(msg)
				if err != nil {
					log.Printf("error: %v", err)
					client.Close()
					delete(Clients, client)
				}
			}(client)
		}
	}
}

func main() {
	// Get port from args
	args := os.Args
	if len(args) != 2 {
		fmt.Println("I need only one arg...exit")
		os.Exit(1)
	}
	if _, err := strconv.Atoi(args[1]); err != nil {
		fmt.Println("You give me not a digit...exit")
		os.Exit(1)
	}
	port := args[1]

	// Create a simple file server
	fileServer := http.FileServer(http.Dir("./public"))
	http.Handle("/", fileServer)

	// Configure websocket route
	http.HandleFunc("/ws", handleConnections)

	// Start listening for incoming chat messages
	go handleMessages()

	// Start the server on localhost and log any errors
	fmt.Println(fmt.Sprintf("http server started on :%s", port))
	err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	HandleErrors(err, fmt.Sprintf("http server did't started on :%s port", port))
}

func HandleErrors(err error, msg string) {
	if err != nil {
		fmt.Println(err, msg)
		log.Fatal("ListenAndServe: ", err, msg)
	}
}
